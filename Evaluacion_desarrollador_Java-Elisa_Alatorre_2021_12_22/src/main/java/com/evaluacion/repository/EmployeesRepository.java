package com.evaluacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.evaluacion.model.Employees;


public interface EmployeesRepository extends CrudRepository<Employees, Integer>{
	
	@Query("select * from employees where name like '%?1%' and las_name like '%?2%'")
	Employees findByNameAndLastName(String name,String lastName);
	
	@Query("select * from employees where id_job = ?1")
	List<Employees> findByIdJob(Integer id);
}
