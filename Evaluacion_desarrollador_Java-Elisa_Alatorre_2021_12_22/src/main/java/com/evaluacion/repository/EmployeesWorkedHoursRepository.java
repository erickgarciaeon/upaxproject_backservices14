package com.evaluacion.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.evaluacion.model.Employees_Worked_Hours;

public interface EmployeesWorkedHoursRepository extends CrudRepository<Employees_Worked_Hours, Integer>{
	
	@Query("select sum(worked_hours) from employee_worked_hours where id_employee = ?1")
	Integer findHoursWorked(Integer id);
	
	@Query("select * from employee_worked_hours where worked_hours ='?1'")
	Employees_Worked_Hours findEqualHour(Date date);
	
	@Query("select sum(worked_hours) from employee_worked_hours where id_employee = ?1 and worked_date between '?2' and '?3'")
	Integer findHoursByRank(Integer id, Date startDate, Date endDate);
}
