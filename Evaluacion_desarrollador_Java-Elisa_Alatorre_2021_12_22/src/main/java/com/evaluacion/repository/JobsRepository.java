package com.evaluacion.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.evaluacion.model.Jobs;

public interface JobsRepository extends CrudRepository<Jobs, Integer>{
	
	@Query("select sum(jobs.salary)  from jobs jobs inner join employees emp on emp.id_job = jobs.id_job "
			+ "inner join employee_worked_hours ewh on ewh.id_employee = emp.id_employee "
			+ "where emp.id_employee = ?1 and ewh.worked_date between '?2' and '?3';")
	Integer findPayment(Integer id, Date startDate, Date endDate);
}
