package com.evaluacion.repository;

import org.springframework.data.repository.CrudRepository;

import com.evaluacion.model.Genders;

public interface GendersRepository extends CrudRepository<Genders, Integer> {

}
