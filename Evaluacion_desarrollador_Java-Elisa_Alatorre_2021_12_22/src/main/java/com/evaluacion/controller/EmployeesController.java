package com.evaluacion.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.evaluacion.model.Employees;
import com.evaluacion.model.Employees_Worked_Hours;
import com.evaluacion.service.EmployeesService;
import com.evaluacion.service.EmployeesWorkedHoursService;
import com.evaluacion.service.GendersService;
import com.evaluacion.service.JobsService;

@RestController
@RequestMapping("employees")
public class EmployeesController {

	@Autowired
	private EmployeesWorkedHoursService employeeWorkedHoursService;
	private Date date = new Date(System.currentTimeMillis());
	private EmployeesService employeeService;
	private GendersService gendersService;
	private JobsService jobsService;
	
	@PostMapping("/creatEmployee")
	public @ResponseBody ResponseEntity<String> creatEmployee(@RequestBody Employees employee) {
		Employees employeeCreated = null;
		try {
			if(!employeeService.getExistNameLasName(employee.getName(), employee.getLastName())&&employeeService.validAge(employee.getBirthdate().toString())&&gendersService.getExist(employee.getGender_id().getId_gender())&&jobsService.getExist(employee.getJos_id().getId_job())) {
				employeeCreated = employeeService.creat(employee);
				return new ResponseEntity<>("id:".concat(employeeCreated.getId_employee().toString()), HttpStatus.OK);
			}// if
			else
				return new ResponseEntity<>("id:".concat("null"), HttpStatus.BAD_REQUEST);

		}// try
		catch (Exception e) {
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error creating employee", e);
		}//catch
	}// creatEmployee
	
	@PostMapping("/addWorkedHours")
	public @ResponseBody ResponseEntity<String> addWorkedHours(@RequestBody Employees_Worked_Hours employeeWH) {
		Employees_Worked_Hours employeeWHCreated= null;
		try {
			if(employeeService.getById(employeeWH.getEmployee_id().getId_employee()) && employeeWorkedHoursService.getLessHours(employeeWH.getEmployee_id().getId_employee()) &&(employeeWH.getWorked_date().before(date)||employeeWH.getWorked_date().equals(date))&& employeeWorkedHoursService.getEqualHours(employeeWH.getWorked_date())) {
				employeeWHCreated = employeeWorkedHoursService.creatEmployeeWH(employeeWH);
				return new ResponseEntity<>("id:".concat(employeeWHCreated.getId_employee_worked_hour().toString()), HttpStatus.OK);
			}// if
			else
				return new ResponseEntity<>("id:".concat("null"), HttpStatus.BAD_REQUEST);
		}// try
		catch (Exception e) {
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error creating employee", e);
		}// catch
						
	}// addWorkedHours
	
	@GetMapping("/consultEmployeesJobs")
	public @ResponseBody ResponseEntity<List<Employees>> consultEmployeesJobs(@RequestParam(value = "job_id") Integer id){
		List<Employees> employeesList = null;
		try{
			if(jobsService.getExist(id))
				employeesList = employeeService.getByIdJob(id);
		}catch (Exception e) {
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error creating employee", e);
		}
		return new ResponseEntity<>(employeesList, HttpStatus.OK);
	}//consultEmployeesJobs
	
	@GetMapping("/consultHoursWorked")
	public @ResponseBody ResponseEntity<String> consultHoursWorked(@RequestParam(value = "employee_id") Integer id, @RequestParam(value = "start_date") String startDate, @RequestParam(value = "end_date") String endDate){
		Integer hours = 0;
		Date star_date = new Date(startDate);
		Date end_date = new Date(endDate);
		try{
			if(employeeService.getById(id) && star_date.before(end_date)){
				hours = employeeWorkedHoursService.getHoursByIdEmployee(id, star_date, end_date);
				return new ResponseEntity<>("total_worked_hours:"+hours.toString(), HttpStatus.OK);
			}//if
			else
				return new ResponseEntity<>("total_worked_hours: null", HttpStatus.BAD_REQUEST);
		}// if
		catch (Exception e) {
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error creating employee", e);
		}// catch
	}//consultHoursWorked
	
	@GetMapping("/consultPayment")
	public @ResponseBody ResponseEntity<String> consultPayment(@RequestParam(value = "employee_id") Integer id, @RequestParam(value = "start_date") String startDate, @RequestParam(value = "end_date") String endDate){
		Integer payment = 0;
		Date star_date = new Date(startDate);
		Date end_date = new Date(endDate);
		try{
			if(employeeService.getById(id) && star_date.before(end_date)){
				payment = jobsService.getPayment(id, star_date, end_date);
				return new ResponseEntity<>("Payment:"+payment.toString(), HttpStatus.OK);
			}//if
			else
				return new ResponseEntity<>("Payment: null", HttpStatus.BAD_REQUEST);
		}// if
		catch (Exception e) {
			throw new ResponseStatusException(
			           HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error creating employee", e);
		}// catch
	}//consultPayment
}
