package com.evaluacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluacionDesarrolladorJavaElisaAlatorre20211222Application {

	public static void main(String[] args) {
		SpringApplication.run(EvaluacionDesarrolladorJavaElisaAlatorre20211222Application.class, args);
	}

}
