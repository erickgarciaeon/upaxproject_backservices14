package com.evaluacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Jobs {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id_job;
	
	@Column(name="name", nullable=false, length=255)
	private String name;
	
	@Column(name="salary", nullable=false, length=255)
	private Float salary;

	public Integer getId_job() {
		return id_job;
	}
	public void setId_job(Integer id_job) {
		this.id_job = id_job;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getSalary() {
		return salary;
	}
	public void setSalary(Float salary) {
		this.salary = salary;
	}
}
