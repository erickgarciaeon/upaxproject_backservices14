package com.evaluacion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Employees_Worked_Hours {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id_employee_worked_hour;
	
	@Column (nullable=false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Integer worked_hours;
	
	@Column(nullable=false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date worked_date;
	
	@ManyToOne
	@JoinColumn(name="id_employee", referencedColumnName = "id_employee")
	private Employees employee_id;

	public Integer getId_employee_worked_hour() {
		return id_employee_worked_hour;
	}
	public void setId_employee_worked_hour(Integer id_employee_worked_hour) {
		this.id_employee_worked_hour = id_employee_worked_hour;
	}
	public Integer getWorked_hours() {
		return worked_hours;
	}
	public void setWorked_hours(Integer worked_hours) {
		this.worked_hours = worked_hours;
	}
	public Date getWorked_date() {
		return worked_date;
	}
	public void setWorked_date(Date worked_date) {
		this.worked_date = worked_date;
	}
	public Employees getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(Employees employee_id) {
		this.employee_id = employee_id;
	}
}
