package com.evaluacion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Employees {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id_employee;
	
	@Column(name="name", nullable=false, length=255) 
	private String name;
	
	@Column(name="last_name", nullable=false, length=255)
	private String lastName;
	
	@Column(name="birthdate", nullable=false)
	@JsonProperty("birth")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date birthdate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_gender", referencedColumnName = "id_gender")
	private Genders gender_id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_job", referencedColumnName = "id_job")
	private Jobs jos_id;

	public Integer getId_employee() {
		return id_employee;
	}
	public void setId_employee(Integer id_employee) {
		this.id_employee = id_employee;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public Genders getGender_id() {
		return gender_id;
	}
	public void setGender_id(Genders gender_id) {
		this.gender_id = gender_id;
	}
	public Jobs getJos_id() {
		return jos_id;
	}
	public void setJos_id(Jobs jos_id) {
		this.jos_id = jos_id;
	}
}
