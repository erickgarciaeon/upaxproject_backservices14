package com.evaluacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Genders {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id_gender;
	
	@Column(name="name", nullable=false, length=255)
	private String name;

	public Integer getId_gender() {
		return id_gender;
	}
	public void setId_gender(Integer id_gender) {
		this.id_gender = id_gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
