package com.evaluacion.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evaluacion.model.Jobs;
import com.evaluacion.repository.JobsRepository;

@Service
public class JobsService {

	@Autowired
	private JobsRepository jobsDAO;
	
	public Boolean getExist(Integer id) {
		Optional<Jobs> jobsFind = jobsDAO.findById(id);
		return jobsFind.isPresent();
	}
	
	public Integer getPayment(Integer id, Date startDate, Date endDate) {
		return jobsDAO.findPayment(id, startDate, endDate);
	}
}
