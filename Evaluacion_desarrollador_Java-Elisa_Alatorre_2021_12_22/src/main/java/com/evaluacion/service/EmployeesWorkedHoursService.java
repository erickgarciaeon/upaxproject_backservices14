package com.evaluacion.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evaluacion.model.Employees_Worked_Hours;
import com.evaluacion.repository.EmployeesWorkedHoursRepository;

@Service
public class EmployeesWorkedHoursService {

	@Autowired
	private EmployeesWorkedHoursRepository employeesWHDAO;
	private static final Integer VALIDHOURS = 20;
	
	public Employees_Worked_Hours creatEmployeeWH(Employees_Worked_Hours employeeWH) {
		return employeesWHDAO.save(employeeWH);
	}
	
	public Boolean getEqualHours(Date date) {
		Employees_Worked_Hours employeeWHFindEq = employeesWHDAO.findEqualHour(date);
		return employeeWHFindEq != null;	
	}
	
	public Boolean getLessHours(Integer id) {
		Integer workedHours = employeesWHDAO.findHoursWorked(id);
		return workedHours <= VALIDHOURS;
	}
	
	public Integer getHoursByIdEmployee(Integer id, Date startDate, Date endDate) {
		return employeesWHDAO.findHoursByRank(id, startDate, endDate);
	}
}
