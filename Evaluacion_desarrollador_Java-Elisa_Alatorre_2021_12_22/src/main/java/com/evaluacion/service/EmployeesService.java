package com.evaluacion.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evaluacion.model.Employees;
import com.evaluacion.repository.EmployeesRepository;

@Service
public class EmployeesService {
	@Autowired
	private EmployeesRepository employeesDAO;
	private static final Integer EDADVALIDA = 18;
	
	public Employees creat(Employees employee) {
		return employeesDAO.save(employee);
	}
	
	public Boolean getExistNameLasName(String name, String lastName) {
		EmployeesRepository employeeFind=(EmployeesRepository) employeesDAO.findByNameAndLastName(name, lastName);
		return employeeFind != null;
	}
	
	public Boolean validAge(String birthdate) {
        Boolean validAge = Boolean.FALSE;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate fechaNacimiento = LocalDate.parse(birthdate, formatter);
        Period edad = Period.between(fechaNacimiento, LocalDate.now());
        if (edad.getYears() >= EDADVALIDA) {
        	validAge = Boolean.TRUE;
        }
        return validAge;
    }
	
	public List<Employees> getByIdJob(Integer id){
		return employeesDAO.findByIdJob(id);
	}
	
	public boolean getById(Integer id) {
		Optional<Employees> employees = employeesDAO.findById(id);
		return employees.isPresent();
	}
}
