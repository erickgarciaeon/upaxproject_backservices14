package com.evaluacion.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evaluacion.model.Genders;
import com.evaluacion.repository.GendersRepository;

@Service
public class GendersService {

	@Autowired
	private GendersRepository gendersDAO;
	
	public Boolean getExist(Integer id) {
		Optional<Genders> genderFind = gendersDAO.findById(id);
		return genderFind.isPresent();
	}
}
